package main

import (
	"fmt"
	"time"
)

func doSome(number int) {
	time.Sleep(time.Millisecond * 100)
	fmt.Printf("result %d\n", number)
}

func doSomeBack(number int) string {
	time.Sleep(time.Millisecond * 100)
	return fmt.Sprintf("result %d\n", number)
}

func asyncDoSomeBack(number int, resCh chan string) {
	time.Sleep(time.Millisecond * 100)
	resCh <- fmt.Sprintf("result %d\n", number)
}

func main() { // main goroutine
	// 未用go
	start := time.Now()
	for i := 0; i < 10; i++ {
		doSome(13)
	}
	fmt.Println("use time: ", time.Since(start))
	fmt.Println("--------------------")
	// 并发go的写法
	start = time.Now()
	for i := 0; i < 10; i++ {
		// go doSome(13) // 启用子goroutine
		go func(number int) {
			time.Sleep(time.Millisecond * 100)
			fmt.Printf("result %d\n", number)
		}(13)
	}
	time.Sleep(time.Millisecond * 109)
	fmt.Println("go use time: ", time.Since(start)) // 节省了10倍
	fmt.Println("同步：", doSomeBack(13))
	// 如何获得子goroutine的返回 通过chan
	resCh := make(chan string) // unbuffer
	go func() {                // 匿名func
		resCh <- "yzzy"
	}()
	res := <-resCh
	fmt.Println(res)
	fmt.Println("如果我们需要子goroutine返回的内容,这时我们就需要一个chan作为形参的函数")
	fmt.Println("异步 chan")
	resCh2 := make(chan string)
	// go asyncDoSomeBack(13, resCh2) // 子goroutine中写入
	go func(number int, resCh chan string) {
		time.Sleep(time.Millisecond * 100)
		resCh <- fmt.Sprintf("result %d\n", number)
	}(13, resCh2)
	res2 := <-resCh2
	fmt.Println("从子goroutine中获取:", res2)
	fmt.Println("End game")
}
