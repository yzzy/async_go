package main

import (
	"fmt"
	"sync"
	"time"
)

func doSome(number int) {
	time.Sleep(time.Millisecond * 100)
	fmt.Printf("result %d\n", number)
}

func asyncDoSome(number int, wg *sync.WaitGroup) {
	time.Sleep(time.Millisecond * 100)
	fmt.Printf("result %d\n", number)
	wg.Done() // -1
}

func main() {
	start := time.Now()
	wg := &sync.WaitGroup{}
	// add done wait
	// wg.Add(len)
	for i := 0; i < 10; i++ {
		wg.Add(1)
		// go asyncDoSome(13,wg)
		go func(number int, wg *sync.WaitGroup) {
			time.Sleep(time.Millisecond * 100)
			fmt.Printf("result %d\n", number)
			wg.Done()
		}(13, wg)
	}

	wg.Wait()
	// time.Sleep(time.Millisecond * 109) // 现实中这种代码不允许
	fmt.Println("use: time : ", time.Since(start))
	fmt.Println("end game")
}
