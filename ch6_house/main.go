package main

import (
	"fmt"
	"log"
	"sync"
	"time"
)

// 调查房子价值
// 1. 去找物业，查出住户可以是多人 Owners 小王
// 2. 评价房产淘宝法拍 	Price 小李
// 3. 去银行调查是否有贷款未还 返回卡号的数组 BankAccounts 小张
// 4. 计算住户可以拿到的钱 Price - BankMoney GetMoney
// 5. 存入数据库

// 1，2,3可以并发的 4必须是23全部结束才能执行 5必须是4
// 并发 1 2 3

// 调查房屋的信息
type HouseInfo struct {
	ID           int      // 房屋ID 房屋唯一标识 各个系统通用
	Owners       []string // 住户
	Price        int      // 法拍估价
	BankAccounts []int    // 有未还完的房贷
	OwnersMoney  int      // 注意这个可以是负值
}

const Owners = "Owners"
const Price = "Price"
const BankAccounts = "BankAccounts"

type Response struct {
	data map[string]any // int []string []int
	err  error
}

// 1.2.3并发
func sellHouseInfo(id int) (*HouseInfo, error) {
	respch := make(chan Response, 3)
	wg := &sync.WaitGroup{}
	wg.Add(3)
	go tenementInfoForOwners(id, respch, wg)
	go taobaoForPrice(id, respch, wg)
	go BankForBankAccount(id, respch, wg)
	wg.Wait()
	close(respch)
	houseInfo := &HouseInfo{}
	responseMap := make(map[string]any, 3)

	for resp := range respch {
		if resp.err != nil {
			return nil, resp.err
		}
		// 提取data中的内容
		for key, value := range resp.data {
			responseMap[key] = value
		}
	}
	// 更新结构体的内容
	houseInfo.Owners = responseMap[Owners].([]string)
	houseInfo.Price = responseMap[Price].(int)
	houseInfo.BankAccounts = responseMap[BankAccounts].([]int)
	return houseInfo, nil
}

func BankForBankAccount(id int, respch chan Response, wg *sync.WaitGroup) {
	time.Sleep(time.Millisecond * 2023)
	cardIds := []int{1312121, 1212372, 1923312}
	respch <- Response{
		data: map[string]any{BankAccounts: cardIds},
		err:  nil,
	}
	// work down
	wg.Done()
}

func taobaoForPrice(id int, respch chan Response, wg *sync.WaitGroup) {
	time.Sleep(time.Millisecond * 2023)
	respch <- Response{
		data: map[string]any{Price: 400000},
		err:  nil,
	}
	// work down
	wg.Done()
}

func tenementInfoForOwners(id int, respch chan Response, wg *sync.WaitGroup) {
	time.Sleep(time.Millisecond * 2023)
	users := []string{"yzzy", "yzzy's shadow"}
	respch <- Response{
		data: map[string]any{Owners: users},
		err:  nil,
	}
	// work down
	wg.Done()
}

// 4
func unpayHouseloan(cards []int) int {
	return len(cards) * 200000
}

// 5
func storeData(house *HouseInfo) {
	fmt.Printf("%+v\n", house)
	fmt.Println("store data success")
}

func main() {
	start := time.Now()
	houseInfo, err := sellHouseInfo(132324324)
	if err != nil {
		log.Fatal(err)
	}
	// 4
	houseInfo.OwnersMoney = houseInfo.Price - unpayHouseloan(houseInfo.BankAccounts)
	storeData(houseInfo)
	fmt.Println("fetching house info use time : ", time.Since(start))
}
