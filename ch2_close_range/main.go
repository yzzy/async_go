package main

import (
	"fmt"
	"time"
)

func main() {
	messageCh := make(chan int, 100)
	for i := 0; i < 10; i++ {
		messageCh <- i
	}
	// 不加deadlock
	close(messageCh)
	// range 进行读取
	// for item := range messageCh {
	// println("message number: ", item)
	// }
	// range 模拟
	for {
		time.Sleep(time.Second)
		item, ok := <-messageCh
		if !ok {
			break
		}
		fmt.Printf("item: %d, ok : %v\n", item, ok)
	}
	fmt.Println("Game end")
}
