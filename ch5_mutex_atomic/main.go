package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

type Counter struct {
	// sync.Mutex
	number int32
}

func (c *Counter) AddNumber(i int) {
	// c.Mutex.Lock()
	// defer c.Mutex.Unlock()
	atomic.AddInt32(&c.number, int32(i))
}

// 做data race 检查 在go test package
// 10+9+8+7...+1
func main() {
	counter := &Counter{}
	wg := &sync.WaitGroup{}
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(i int) {
			counter.AddNumber(i)
			wg.Done()
		}(i)
	}
	wg.Wait() // 所有的goroutine 结束
	fmt.Printf("%+v", counter)
}
