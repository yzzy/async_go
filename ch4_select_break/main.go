package main

import (
	"fmt"
	"time"
)

type MessageServer struct {
	messageCh chan string
	quitCh    chan struct{}
}

func NewMessageServer() *MessageServer {
	return &MessageServer{
		quitCh:    make(chan struct{}),
		messageCh: make(chan string, 100),
	}
}

func sendMessage(s *MessageServer, message string, number int) {
	for i := 0; i < number; i++ {
		s.messageCh <- fmt.Sprintf("mes: %s: %d", message, i+1)
	}
}

func (s *MessageServer) handleMessage(message string) {
	fmt.Println("get message : ", message)
}

// loop 死循环 工作循环
func (s *MessageServer) work() {
messageloop:
	for {
		select {
		case message := <-s.messageCh:
			s.handleMessage(message)
		case <-s.quitCh:
			fmt.Println("quiting server")
			break messageloop
			// default:
			// fmt.Println("nothing")
		}
	}
	fmt.Println("server is down")
}

func (s *MessageServer) quit() {
	s.quitCh <- struct{}{}
}

func main() {
	server := NewMessageServer()
	go func() {
		time.Sleep(time.Second * 3)
		// server.quitCh <- struct{}{} // 0 byte
		server.quit()
	}()
	sendMessage(server, "house message", 10)
	server.work()
}
